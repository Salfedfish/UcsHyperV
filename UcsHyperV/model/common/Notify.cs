﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UcsHyperV.model.common
{
    internal class Notify
    {
        public int task_id { get; set; }
        public int progress { get; set; }
        public string notify_message { get; set; }


    }
}
