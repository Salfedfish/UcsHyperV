﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UcsHyperV.model.common
{
    internal class BaseTask
    {
        public int task_id { get; set; }
        public string? action { get; set; }
        public string? action_time { get; set; }
        public int ucs_instance_id { get; set; }    
        public int ucs_master_id { get; set; }
        public int type { get; set; }
    }
}
