﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using UcsHyperV.model.common;

namespace UcsHyperV.helper
{
    internal class HttpServer
    {
        private HttpListener _listener;
        public static int _port;
        public bool Status()
        {
            return _listener.IsListening;
        }

        public HttpServer(int port)
        {
            _port = port;
        }

        public void Start()
        {
            _listener = new HttpListener();
            _listener.Prefixes.Add(String.Format("http://*:{0}/", _port.ToString()));
            _listener.Start();
            _listener.BeginGetContext(ProcessRequest, null);
        }

        public void Stop()
        {
            _listener.Stop();
        }

        private void Action(string data)
        {
            try
            {
                var task = new JsonObject(data);
                LogHelper.Info("接受参数:");
                switch (task.GetString("action"))
                {
                    case "create":
                        HyperVHelper.Create(task);
                        break;
                    case "restart":
                        HyperVHelper.ReStart(task);
                        break;
                    case "start":
                        HyperVHelper.Start(task);
                        break;
                    case "shutdown":
                        HyperVHelper.Shutdown(task);
                        break;
                    case "reset_system":
                        HyperVHelper.ReSetSystem(task);
                        break;
                }
                LogHelper.Info("test");
                LogHelper.Info("{0}", task);
            }
            catch (Exception e)
            {
                LogHelper.Info("Data Type Error:{0}\n{1}", data, e.Message); 
            }
        }
        private void ProcessRequest(IAsyncResult result)
        {
            HttpListenerContext context = null;
            try
            {
                _listener.BeginGetContext(ProcessRequest, null);
                context = _listener.EndGetContext(result);

                // 发送短信内容
                var reader = new System.IO.StreamReader(context.Request.InputStream);
                string data = reader.ReadToEnd();
                new Thread(() =>
                {
                    Action(data);
                }).Start();
                // Http回复
                using (StreamWriter writer = new StreamWriter(context.Response.OutputStream))
                {
                    context.Response.StatusCode = 200;
                    JObject json = new JObject();
                    json.Add("status", true);
                    json.Add("message", "Success");
                    writer.Write(json.ToString());
                    writer.Close();
                }
                context.Response.StatusCode = 200;
                context.Response.Close();
            }
            catch (Exception e)
            {
                using (StreamWriter writer = new StreamWriter(context.Response.OutputStream))
                {
                    context.Response.StatusCode = 400;
                    writer.Write(e.Message);
                    writer.Close();
                }
            }
        }
    }
}