﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace UcsHyperV.helper
{
    internal class ConfigHelper
    {
        private static string path = Environment.CurrentDirectory+"\\hiy.ini";
        [DllImport("kernel32")] // 写入配置文件的接口
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")] // 读取配置文件的接口
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);
        // 向配置文件写入值
        public static void set(string section, string key, string value)
        {
            WritePrivateProfileString(section, key, value, path);
        }
        // 读取配置文件的值
        public static string get(string section, string key)
        {
            StringBuilder sb = new StringBuilder(255);
            GetPrivateProfileString(section, key, "", sb, 255, path);
            return sb.ToString().Trim();
        }
    }
}