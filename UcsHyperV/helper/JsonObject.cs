﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UcsHyperV.helper
{
    internal class JsonObject
    {
        private JObject json_object;
        public JsonObject()
        {

            json_object = new JObject();
        }
        public override string ToString()
        {
            return json_object.ToString();
        }
        public void SetJsonArray(string key ,JArray value) {
            json_object.Add(key, value);    
        }
        public JsonObject(string json)
        {
            json_object = JObject.Parse(json);
        }
        internal int GetInt(string key)
        {
            if (json_object.ContainsKey(key))
            {
                var str = json_object. GetValue(key);
                if (str != null)
                {
                    try
                    {
                       return Convert.ToInt32(str.ToString());
                    }
                    catch { }
                }

            }
            return 0;
        }
        public JsonArray? GetJsonArray(string key)
        {
            if (json_object.ContainsKey(key))
            {
                var str = json_object.GetValue(key);
                if (str != null)
                {
                    return new JsonArray(str.ToString());
                }

            }
            return null;
        }
        public JsonObject? GetJsonObject(string key)
        {

            if (json_object.ContainsKey(key))
            {
                var str = json_object.GetValue(key);
                if (str != null)
                {
                    return new JsonObject(str.ToString());
                }
            }
            return null;
        }
        internal string? GetString(string key)
        {

            if (json_object.ContainsKey(key))
            {

                var str = json_object. GetValue(key);
                if (str != null)
                {
                    return str.ToString();
                }
            }
            return null;
        }
    }
}