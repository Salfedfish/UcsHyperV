﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Text;
using System.Threading.Tasks;

namespace UcsHyperV.helper
{
    internal class HyperVHelper
    {

        public static void Create(JsonObject task) {
            int task_id = task.GetInt("task_id");
            LogHelper.Info("创建实例-任务ID:{0}", task_id);
            int instance_id = task.GetInt( "instance_id");

            int cpu = task.GetInt( "cpu");
            int cpu_ratio = task.GetInt( "cpu_ratio");
            int memory = task.GetInt( "memory");
            int bandwidth = task.GetInt( "bandwidth");

            var public_ip = task.GetJsonArray("ip_address");

            var dns = task.GetJsonArray("dns");

            var private_ip = new JArray();
            foreach (var ip in public_ip.GetJArray())
            {
                var t_ip = new JObject();
                var temp = ip.ToObject<JObject>();

                var ip_addr = temp.GetValue("ip").ToString().Split('.');
                t_ip.Add("ip", "10." + ip_addr[1] + "." + ip_addr[2] + "." + ip_addr[3]);
                t_ip.Add("netmask", "255.0.0.0");
                private_ip.Add(t_ip);
            }
            task.SetJsonArray("private_ip",private_ip);
            LogHelper.Info("创建实例-任务ID:{0}", task_id);
            var flag = false;


            PSHelper.CreateConfigISO(task_id,instance_id,5,task);

            LogHelper.Info("创建-任务ID:{0}", task_id);
            //创建虚拟机
            flag = PSHelper.CreateInstance(task_id,instance_id, cpu, cpu_ratio, memory,bandwidth,task);
            if (!flag)
            {
                LogHelper.Info("Create Instance Error!");
                return;
            }

            //创建虚拟机磁盘
            string temp_name = task.GetString( "mirror_name");
            if (temp_name==null) {
                return;
            }
            if (flag)
            {
                //系统盘创建成功，开始创建数据盘
                var disks = task.GetJsonArray("harddisk").GetJArray();
                if (disks != null)
                {
                    LogHelper.Info("有磁盘!");
                    double progress_ratio = (20.00 / disks.Count);

                    for (int i = 0; i < disks.Count; i++)
                    {
                        var item =new JsonObject(disks[i].ToString());
                        if (disks != null)
                        {
                            if (item.GetInt("type") == 1)
                            {
                                var system_disk_path = item.GetString("path");
                                var disk_config = item.GetJsonObject("storage_config");
                                var template_path = disk_config.GetString("template_path");
                                var suffix = disk_config.GetString("suffix");
                                flag = PSHelper.CreateSystemDisk(task_id, instance_id, template_path + "\\" + temp_name+"\\"+temp_name+ suffix, system_disk_path, 50);
                                // Add-VMHardDiskDrive -VMName 1 -ControllerType IDE -ControllerLocation 0 -Path C:\Hyper-V\data\1.vhdx
                                PSHelper.MountSystemDisk(task_id, instance_id,@system_disk_path, 60);
                            }
                            else
                            {
                                int size = item.GetInt("size");
                                string path = item.GetString("path");
                                flag = PSHelper.CreateDataDisk(task_id, path, size, 60 + (int)(progress_ratio * (i + 1)));

                                if (!flag)
                                {
                                    LogHelper.Info("Create DataDisk Error!");
                                    break;
                                }
                                flag = PSHelper.MountDataDisk(task_id, path, instance_id, i, 60 + (int)(progress_ratio * (i + 1)));
                                if (!flag)
                                {
                                    LogHelper.Info("Mount DataDisk Error!");
                                    break;
                                }
                            }
                        }
                        LogHelper.Info("{0}", item);
                    }
                }
            }
            if (flag)
            {
                PSHelper.MountConfigISO(task_id, instance_id, 90);
                PSHelper.OnArp(task_id, instance_id,95,task);
                PSHelper.StartInstance(task_id,instance_id,100);
                //
            }

            //Starting

        }

        internal static void ReSetSystem(JsonObject task)
        {
            int task_id = task.GetInt("task_id");
            LogHelper.Info("重装实例-任务ID:{0}", task_id);
            int instance_id = task.GetInt("instance_id");

            PSHelper.ShutdownInstance(task_id, instance_id, 10);

            LogHelper.Info("强制关闭实例-任务ID:{0}", task_id);
            

            PSHelper.CreateConfigISO(task_id, instance_id, 5, task);

            var disks = task.GetJsonArray("harddisk").GetJArray();

            string temp_name = task.GetString("mirror_name");
            for (int i = 0; i < disks.Count; i++)
            {
                var item = new JsonObject(disks[i].ToString());
                var system_disk_path = item.GetString("path");
                var disk_config = item.GetJsonObject("storage_config");
                var template_path = disk_config.GetString("template_path");
                var suffix = disk_config.GetString("suffix");
                var flag = PSHelper.CreateSystemDisk(task_id, instance_id, template_path + "\\" + temp_name + "\\" + temp_name + suffix, system_disk_path, 50);
                PSHelper.MountSystemDisk(task_id, instance_id, @system_disk_path, 60);
            } 
            PSHelper.MountConfigISO(task_id, instance_id, 90);

        }

        internal static void Start(JsonObject task)
        {
            int task_id = task.GetInt("task_id");
            LogHelper.Info("启动实例-任务ID:{0}", task_id);
            int instance_id = task.GetInt("instance_id");
            PSHelper.StartInstance(task_id, instance_id, 100);
        }

        public static void ReStart(JsonObject task)
        {
            int task_id = task.GetInt("task_id");
            LogHelper.Info("重启实例-任务ID:{0}", task_id);
            int instance_id = task.GetInt("instance_id");
            PSHelper.ReStartInstance(task_id, instance_id, 100);
        }

        public static void Shutdown(JsonObject task)
        {
            int task_id = task.GetInt("task_id");
            LogHelper.Info("Shutdown实例-任务ID:{0}", task_id);
            int instance_id = task.GetInt("instance_id");
            PSHelper.ShutdownInstance(task_id, instance_id, 100);
        }

        public static void Delete(JObject task) { 
        
        }
    }
}
