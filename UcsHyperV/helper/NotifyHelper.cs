﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UcsHyperV.model.common;

namespace UcsHyperV.helper
{
    internal class NotifyHelper
    {
        public static void Notify(Notify notify)
        {
            LogHelper.Info(notify.notify_message);
            Notify(notify.task_id, notify.progress, notify.notify_message);
        }
        public static void Notify(int task_id, int progress, string notify_message)
        {
            Console.WriteLine(notify_message);
            string data = Post("/notify/ucs/notify", new Dictionary<string, object>() {
                { "task_id",""+task_id   } ,{ "progress",progress}    ,{ "notify_message",notify_message}      });
            Console.WriteLine(data);
        }
        /// 创建POST方式的HTTP请求  
        public static string Post(string url, IDictionary<string, object> parameters, int timeout = 30, string userAgent = null, CookieCollection cookies = null)
        {
            try
            {
                HttpWebRequest request = null;
                url = Config.api + url;
                //如果是发送HTTPS请求  
                if (url.StartsWith("https", StringComparison.OrdinalIgnoreCase))
                {
                    request = WebRequest.Create(url) as HttpWebRequest;
                }
                else
                {
                    request = WebRequest.Create(url) as HttpWebRequest;
                }
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";

                //设置代理UserAgent和超时
                //request.UserAgent = userAgent;
                //request.Timeout = timeout; 

                if (cookies != null)
                {
                    request.CookieContainer = new CookieContainer();
                    request.CookieContainer.Add(cookies);
                }
                //发送POST数据  
                if (!(parameters == null || parameters.Count == 0))
                {
                    StringBuilder buffer = new StringBuilder();
                    int i = 0;
                    foreach (string key in parameters.Keys)
                    {
                        if (i > 0)
                        {
                            buffer.AppendFormat("&{0}={1}", key, parameters[key]);
                        }
                        else
                        {
                            buffer.AppendFormat("{0}={1}", key, parameters[key]);
                            i++;
                        }
                    }
                    byte[] data = Encoding.ASCII.GetBytes(buffer.ToString());
                    using (Stream stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
                }
                string[] values = request.Headers.GetValues("Content-Type");
                var webresponse = request.GetResponse() as HttpWebResponse;

                using (Stream s = webresponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(s, Encoding.UTF8);
                    return reader.ReadToEnd();
                }
            }
            catch
            {
                Console.WriteLine();
                return null;
            }
        }

    }
}