﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UcsHyperV.helper
{
    internal class Config
    {
        //公网网卡名称
        public static string? public_switch { get; set; }
        //内网网卡名称
        public static string? private_switch { get; set; }
        //实例存放位置
        public static string? instance_path { get; set; }
        //临时文件存放位置
        public static string? temp_path { get; set; }

        //程序运行端口
        public static int port { get; set; }
        public static string api { get; set; }
        public static void Initialize()
        {
            try
            {
                port = Convert.ToInt32(ConfigHelper.get("node", "port"));
                if (port < 0 || port > 65535)
                {
                    Console.WriteLine("端口范围只能为:1-65535 已为您设置为9981端口");
                }
                else
                {
                    port = 9981;
                }
            }
            catch
            {
                port = 9981;
                Console.WriteLine("端口范围只能为:1-65535 已为您设置为9981端口");
            }
            public_switch = ConfigHelper.get("node", "public_switch");
            LogHelper.Info("公网网卡:{0}", public_switch);
            private_switch = ConfigHelper.get("node", "private_switch");
            LogHelper.Info("内网网卡:{0}", private_switch); 
            instance_path = ConfigHelper.get("node", "instance_path");
            LogHelper.Info("实例存放路径:{0}", instance_path);


            temp_path = ConfigHelper.get("node", "temp_path");
            LogHelper.Info("临时文件存放路径:{0}", temp_path);
            api = ConfigHelper.get("node", "api");
            LogHelper.Info("API地址:{0}", api);


        }
    }
}