﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UcsHyperV.helper
{
    internal class LogHelper
    {

        public static void Info(string message, params object[] obj)
        {
            Console.WriteLine("["+DateTime.Now+"]:"+message, obj);
        }
    }
}