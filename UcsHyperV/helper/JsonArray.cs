﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UcsHyperV.helper
{
    internal class JsonArray 
    {
        private JArray jArray;
        public JsonArray(string json)
        {
            jArray = JArray.Parse(json);
        }
        public JArray GetJArray() {

            return jArray;
        }
        public string ToString()
        {
            return jArray.ToString();
        }

    }
}
